
# Install dependencies

## exodriver

`git clone git://github.com/labjack/exodriver.git`

`cd exodriver/`

`sudo ./install.sh`

## LabJackPython

`sudo pip install LabJackPython`

