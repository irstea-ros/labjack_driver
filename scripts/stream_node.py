#!/usr/bin/env python
# Copyright 2018 Irstea
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import rospy

from std_msgs.msg import Float32

import ue9


class Labjack(object):
    def __init__(self, channel, resolution=16, scan_freq=366):
        self.channel_name = "AIN"+str(channel)
        self.d = ue9.UE9()
        self.d.getCalibrationData()
        self.d.streamConfig(
            NumChannels=1,
            ChannelNumbers=[channel],
            Resolution=resolution,
            ScanFrequency=scan_freq)

        if self.d is None:
            return

        self.d.streamStart()

        self.pub = rospy.Publisher('chatter', Float32, queue_size=10)

        #timer_period = 0.01  # in second
        #self.timer = rospy.Timer(rospy.Duration(timer_period), self.timer_callback)
        #rospy.spin()

        while rospy.is_shutdown() is False:
            self.timer_callback()

        self.d.streamStop()
        self.d.close()

    def timer_callback(self):
        msg = Float32()
        # Conversion in milli volt for gain 1
        data = next(self.d.streamData())
        if data is not None:
            msg.data = sum(data[self.channel_name])/len(data[self.channel_name])
            rospy.loginfo('Publishing: "{0}"'.format(msg.data))
            self.pub.publish(msg)


class LabjackAIN(object):
    def __init__(self, channel, resolution=16):
        self.channel_name = "AIN"+str(channel)
        self.d = ue9.UE9()
        self.d.getCalibrationData()

        self.pub = rospy.Publisher('chatter', Float32, queue_size=10)

        timer_period = 0.01  # in second
        self.timer = rospy.Timer(rospy.Duration(timer_period), self.timer_callback)
        rospy.spin()

        self.d.close()

    def timer_callback(self, event):
        msg = Float32()
        # Conversion in milli volt for gain 1
        data = self.d.getAIN(1)
        if data is not None:
            msg.data = data
            rospy.loginfo('Publishing: "{0}"'.format(msg.data))
            self.pub.publish(msg)

if __name__ == '__main__':
    rospy.init_node('labjack', anonymous=True)

    node = LabjackAIN(1)